local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
  vim.cmd 'packadd packer.nvim'
end

local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

require('packer_compiled')

require('packer').startup{
	{
		-- Packer can manage itself
		'wbthomason/packer.nvim',

		-- Statusbar, required nvim-web-devicons for cool icons
		{
			'NTBBloodbath/galaxyline.nvim',
			branch = 'main',
			-- your statusline
			-- config = function() require'statusbar' end,
			-- some optional icons
			requires = {'kyazdani42/nvim-web-devicons', opt = true}
		},

		-- Helps remember keybindings
		{
			"folke/which-key.nvim",
			config = function()
				require("which-key").setup {
					plugins = {
						spelling = {
							enabled = true
						}
					}
				}
			end
		},

		-- Better syntax highlighting
		{
			"nvim-treesitter/nvim-treesitter",
			run = ":TSUpdate",
			config = require 'nvim-treesitter.configs'.setup {
				highlight = { enable = true },
				indent = { enable = true },
				rainbow = {
					enable = true,
					extended_mode = true,
					max_file_lines = 10000,
					-- Setting colors
					colors = {
						'#ff0000',
						'#ff7f00',
						'#ffff00',
						'#00ff00',
						'#0000ff',
						'#ff00ff',
					}
				},
				autopairs = {enable = true},
			}
		},
		-- Differentiate pairs
		'p00f/nvim-ts-rainbow',
		-- Show context(e.g. what function you're editing) if not in view in floating window at the top
		{
			'romgrk/nvim-treesitter-context',
			config = require 'treesitter-context'.setup {
				enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
			}
		},
		-- Syntax highlighting for make files, files, etc.
		'gentoo/gentoo-syntax',

		-- Visualize indentation
		{
			"lukas-reineke/indent-blankline.nvim",
			config = require("indent_blankline").setup {
				char = "▏",
				show_current_context = true,
				filetype_exclude = {"man", "help"}
			}
		},

		-- Easily comment and uncomment
		{
		    'numToStr/Comment.nvim',
		    config = require('Comment').setup(),
		},
		{
			'JoosepAlviste/nvim-ts-context-commentstring',
			config =require'nvim-treesitter.configs'.setup {
				context_commentstring = {
					enable = true
				}
			},
		},

		-- Visualize colors
		{
			'norcalli/nvim-colorizer.lua',
			config = require 'colorizer'.setup(nil, { css = true; })
		},

		-- LSP, completion, and snippets
		{
			'neovim/nvim-lspconfig',
			config = function()
				local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }

				for type, icon in pairs(signs) do
					local hl = "DiagnosticsSign" .. type
					vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
				end
			end
		},
		'hrsh7th/cmp-nvim-lsp',
		'hrsh7th/cmp-buffer',
		'rafamadriz/friendly-snippets',
		'L3MON4D3/LuaSnip',
		'saadparwaiz1/cmp_luasnip',
		{
			'hrsh7th/nvim-cmp',
			config = function()
				local cmp = require("cmp")
				local lspkind = require("lspkind")
				local luasnip = require("luasnip")
				cmp.setup {
					snippet = {
						expand = function(args)
							require('luasnip').lsp_expand(args.body)
						end,
					},
					mapping = {
						['<C-d>'] = cmp.mapping.scroll_docs(-4),
						['<C-f>'] = cmp.mapping.scroll_docs(4),
						['<C-Space>'] = cmp.mapping.complete(),
						['<C-e>'] = cmp.mapping.close(),
						['<CR>'] = cmp.mapping.confirm {
							behavior = cmp.ConfirmBehavior.Replace,
							select = true,
						},
						['<Tab>'] = function(fallback)
							if cmp.visible() then
								cmp.select_next_item()
							elseif luasnip.expand_or_jumpable() then
								luasnip.expand_or_jump()
							else
								fallback()
							end
						end,
						['<S-Tab>'] = function(fallback)
							if cmp.visible() then
								cmp.select_prev_item()
							elseif luasnip.jumpable(-1) then
								luasnip.jump(-1)
							else
								fallback()
							end
						end,
					},
					sources = {
						{ name = 'path' },
						{ name = 'buffer' },
						{ name = 'nvim_lsp' },
						{ name = 'luasnip' },
						{ name = 'crates' },
					},
					formatting = {
						format = lspkind.cmp_format({with_text = true, maxwidth = 50})
					},
				}

				-- Advertise nvim-cmp capabilities to lsp servers
				-- Not initializing rust-analyzer here because it is done by rust-tools
				local servers = { 'clangd', 'jedi_language_server' }
				local capabilities = vim.lsp.protocol.make_client_capabilities()
				capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)
				for _, lsp in ipairs(servers) do
					require'lspconfig'[lsp].setup {
						capabilities = capabilities,
					}
				end
			end,
		},
		{
			'onsails/lspkind-nvim',
			config = require('lspkind').init()
		},
		{
			'RishabhRD/lspactions',
			requires = {
				'nvim-lua/plenary.nvim',
				'nvim-lua/popup.nvim',
			}
		},
		{
			'simrat39/symbols-outline.nvim',
		},
		{
			'ray-x/lsp_signature.nvim',
			config = require "lsp_signature".setup(),
		},

		-- Debugging
		{
			'mfussenegger/nvim-dap',
			requires = 'rcarriga/nvim-dap-ui',
		},

		-- Git integration

		{
			'lewis6991/gitsigns.nvim',
			requires = {
				'nvim-lua/plenary.nvim'
			},
			config = require('gitsigns').setup() 
		},

		-- File explorer
		{
		    'kyazdani42/nvim-tree.lua',
		    requires = 'kyazdani42/nvim-web-devicons',
		    config = require'nvim-tree'.setup {}
		},

		-- Onedark theme
		{
			'navarasu/onedark.nvim',
			config = function()
				require('onedark').setup {
					style = 'darker',
					transparent = true,
				}
				
				require('onedark').load()	
			end,
		},

		-- Automagically create pairs
		{
			'windwp/nvim-autopairs',
			config = function()
				require('nvim-autopairs').setup({
					check_ts = true,
				})

				-- Set up nvim-autopairs to work with nvim-cmp
				require('cmp').setup({
					map_cr = true, --  map <CR> on insert mode
					map_complete = true, -- it will auto insert `(` (map_char) after select function or method item
					auto_select = true, -- automatically select the first item
					insert = false, -- use insert confirm behavior instead of replace
					map_char = { -- modifies the function or method delimiter by filetypes
						all = '(',
						tex = '{'
					}
				})
			end,
		},
		-- Create or delete pairs with keybinds
		--[[ {
			'steelsojka/pears.nvim',
			config = 
				require "pears".setup(function(conf)
					conf.on_enter(function(pears_handle)
						if vim.fn.pumvisible() == 1 and vim.fn.complete_info().selected ~= -1 then
							return vim.fn["compe#confirm"]("<CR>")
						else
							pears_handle()
						end
					end)
					conf.preset "tag_matching"
				end)
		}, --]]
		--[[ {
			'blackCauldron7/surround.nvim',
			config = require 'surround'.setup{}
		}, --]]

		'machakann/vim-sandwich',
		'lewis6991/impatient.nvim',
		'nathom/filetype.nvim',

		-- Fuzzy finder
		{
			'nvim-telescope/telescope.nvim',
			requires = 'nvim-lua/plenary.nvim' ,
			config = function()
				require("telescope").setup()
				require("telescope").load_extension("ui-select")
			end
		},
		{
			'nvim-telescope/telescope-fzf-native.nvim',
			run = 'make'
		},
		'sudormrfbin/cheatsheet.nvim',
		{
			'benfowler/telescope-luasnip.nvim',
			config = require("luasnip/loaders/from_vscode").load({ paths = { "~/.local/share/nvim/site/pack/packer/start/friendly-snippets" }})
		},
		{
			'nvim-telescope/telescope-ui-select.nvim'
		},
		
		-- Rust specific plugins
		{
			'saecki/crates.nvim',
			tag = 'v0.1.0',
			requires = { 'nvim-lua/plenary.nvim' },
			config = function()
				require('crates').setup()
			end,
		},
		{
			'simrat39/rust-tools.nvim',
			config = function()
				require('rust-tools').setup({
					tools = {
						hover_actions = {
							auto_focus = true,
						},
					},
					dap = {
						adapter = function()
							local extension_path = '/home/zaheen/Desktop/rust_VSCode_DAP_Extension'
							require('rust-tools.dap').get_codelldb_adapter(extension_path .. 'adapter/codelldb', extension_path .. 'lldb/lib/liblldb.so')
						end,
					},
					server = {
						checkOnSave = {
							command = "clippy"
						},
					}
				})
			end
		},

		{
			'akinsho/bufferline.nvim',
			config = require('bufferline').setup {
				options = {
					numbers = function(opts)
						return string.format('%s.', opts.id)
					end,
					always_show_bufferline = false,
					diagnostics = "nvim_lsp",
					diagnostics_indicator = function(count, level, diagnostics_dict, context)
						local s = " "
						for e, n in pairs(diagnostics_dict) do
							local sym = e == "error" and "   "
								or (e == "warning" and "  " or "  " )
							s = s .. sym .. n
						end
						return s
					end,
					offsets = {
						{
							filetype = "NvimTree",
							--[[ text = function()
								return vim.fn.getcwd()
							end,
							highlight = "Directory",
							text_align = "left" --]]
						},
						{
							filetype  = "dap-repl"
						},
					},
					show_close_icon = false,
				}
			},
		},
	},
	config = {
		-- Move to lua dir so impatient.nvim can cache it
		compile_path = vim.fn.stdpath('config')..'/lua/packer_compiled.lua'
	}
}
