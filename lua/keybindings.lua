-- Setting keybinding
local opts = { buffer = bufnr }
-- Mappings.
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- LSP settings
--[[ local lspconfig = require 'lspconfig'
local on_attach = function(_, bufnr) ]]
vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
vim.keymap.set('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, opts)
vim.keymap.set('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, opts)
vim.keymap.set('n', '<leader>wl', function()
	vim.inspect(vim.lsp.buf.list_workspace_folders())
	end, opts)
vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, opts)
vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, opts)
vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, opts)
vim.keymap.set('n', '<leader>so', require('telescope.builtin').lsp_document_symbols, opts)
vim.api.nvim_create_user_command("Format", vim.lsp.buf.formatting, {})
vim.keymap.set('n', '<leader>f', vim.lsp.buf.formatting, opts)
vim.keymap.set('n', 'gtd', require('telescope.builtin').lsp_type_definitions, opts)
vim.keymap.set('n', '<leader>ff', require('telescope.builtin').find_files, opts)
vim.keymap.set('n', '<leader>fg', require('telescope.builtin').live_grep, opts)
vim.keymap.set('n', '<leader>fb', require('telescope.builtin').buffers, opts)
vim.keymap.set('n', '<leader>fh', require('telescope.builtin').help_tags, opts)
-- vim.keymap.set('n', '<leader>sp', require('telescope.builtin').extensions.luasnip.luasnip, opts)
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist)

vim.keymap.set('n', '<leader>ro', require('rust-tools.open_cargo_toml').open_cargo_toml, opts)
vim.keymap.set('n', '<leader>rp', require('rust-tools.parent_module').parent_module, opts)
vim.keymap.set('n', '<leader>rj', require('rust-tools.join_lines').join_lines, opts)
vim.keymap.set('n', '<leader>rg', require('rust-tools.crate_graph').view_crate_graph, opts)
vim.keymap.set('n', '<leader>sp', require('rust-tools.runnables').runnables, opts)

-- NvimTree
local opts_old = { noremap=true, silent=true }
vim.api.nvim_set_keymap('n', '<C-n>', '<cmd> NvimTreeToggle<CR>', opts_old)
vim.api.nvim_set_keymap('n', '<leader>r', '<cmd> NvimTreeRefresh<CR>', opts_old)
vim.api.nvim_set_keymap('n', '<leader>n', '<cmd> NvimTreeFindFile<CR>', opts_old)

-- See `:help vim.lsp.*` for documentation on any of the below functions
--[[ bmap(bufnr, 'n', '<leader>ff', '<cmd>lua require(\'telescope.builtin\').find_files()<CR>', opts)
bmap(bufnr, 'n', '<leader>fg', '<cmd>lua require(\'telescope.builtin\').live_grep()<CR>', opts)
bmap(bufnr, 'n', '<leader>fb', '<cmd>lua require(\'telescope.builtin\').buffers()<CR>', opts)
bmap(bufnr, 'n', '<leader>fh', '<cmd>lua require(\'telescope.builtin\').help_tags()<CR>', opts)
bmap(bufnr, 'n', '<leader>sp', '<cmd>lua require\'telescope\'.extensions.luasnip.luasnip{}<CR>', opts)
bmap(bufnr, 'n', '<leader>rc', '<cmd>lua require\'rust-tools.open_cargo_toml\'.open_cargo_toml()<CR>', opts)
bmap(bufnr, 'n', '<leader>rp', '<cmd>lua require\'rust-tools.parent_module\'.parent_module()<CR>', opts)
bmap(bufnr, 'n', '<leader>rj', '<cmd>lua require\'rust-tools.join_lines\'.join_lines()<CR>', opts)
bmap(bufnr, 'n', '<leader>rg', '<cmd>lua require\'rust-tools.crate_graph\'.view_crate_graph(backend, output)<CR>', opts)
bmap(bufnr, 'n', '<leader>rr', '<cmd>lua require(\'rust-tools.runnables\').runnables()<CR>', opts) ]]
